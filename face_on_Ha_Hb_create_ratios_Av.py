""" Calculate the Ha to Hb ratio of two images """
import astropy.io.fits
import numpy
import scipy.ndimage
import matplotlib
import matplotlib.pyplot as plt
import os
import sys
sys.path.append(os.environ["ASTRO_TOOLS"]+"/spectools/reddening")
import dereddenLib
import numpy

plt.rcParams["patch.force_edgecolor"] = True
font = {'family' : 'helvetica',
  'size' : 18}
matplotlib.rc('font', **font)

N_angles = 7
deg = numpy.linspace(0,15*(N_angles-1),N_angles)

alpha = [.8, .8, 0.5]
zorder = [5, 0, 10]
line_type = ['-', '-.', 'dotted' ]
color = [(0,0,1,0.5), (1,0,0,0.5), (1.0,1.0,1.0, 0.5)]

mask = {}
Av_maps = {}

for i in range(N_angles):
        dusty_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        nodust_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("nodust", i+1))
        nodust_Hb = astropy.io.fits.open("results/final/em_Hb_%s/int_channel_map_species_0001_line_000%i.fits"%("nodust", i+1))

        ratio = dusty_Ha[0].data[0]/nodust_Ha[0].data[0]
        nodust_HaHb_ratio = nodust_Ha[0].data[0]/nodust_Hb[0].data[0]

        Av_data = dereddenLib.A(dereddenLib.c_ext(ratio, 1.0, flamB=0, intrinsic_ratio=1.0))

        mask[i] = (dusty_Ha[0].data[0] > 1e7)*(Av_data >= 0.0)

        Av_data[Av_data < 0.0] = 0.0
        Av_maps["Ha_Ha0", i] = Av_data*1.0
        dereddened_Ha = dereddenLib.deredden(dusty_Ha[0].data[0], lam=6563., Av=Av_data)

        Av_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        Av_hdul[0].data[0] = Av_data
        Av_hdul.writeto("results/final/Av_Ha_Ha0_ratio_%i.fits"%(i+1), overwrite=True)

        Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        Av_x_Ha_hdul[0].data[0] = Av_data * nodust_Ha[0].data[0]
        Av_x_Ha_hdul.writeto("results/final/AvHa0_x_Ha0_%i.fits"%(i+1), overwrite=True)

        Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        Av_x_Ha_hdul[0].data[0] = Av_data * dereddened_Ha
        Av_x_Ha_hdul.writeto("results/final/AvHa0_x_deredHa_%i.fits"%(i+1), overwrite=True)

        Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_bare_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        Av_x_Ha_hdul[0].data[0] = Av_data * Av_x_Ha_hdul[0].data[0]
        Av_x_Ha_hdul.writeto("results/final/AvHa0_x_Ha_bare_dust_%i.fits"%(i+1), overwrite=True)


        # The following is a non-standard way to calculate Av. Usually it's used to get Ha0 from Av.
        # Av_data    = -2.5 * numpy.log10(dusty_Ha[0].data[0]/nodust_Ha[0].data[0])/0.818

        ratio[numpy.logical_not(mask)] = float(numpy.nan)
        ratio_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))

        ratio_hdul[0].data[0] = ratio
        ratio_hdul.writeto("results/final/Ha_Ha0_ratio_%i.fits"%(i+1), overwrite=True)

        ratio_hdul[0].data[0] = nodust_HaHb_ratio 
        ratio_hdul.writeto("results/final/nodust_Ha_Hb_ratio_%i.fits"%(i+1), overwrite=True)
        
        dered_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
        dered_Ha_hdul[0].data[0] = dereddened_Ha
        dered_Ha_hdul.writeto("results/final/dered_Ha_HaH0_%i.fits"%(i+1), overwrite=True)

        del(dusty_Ha, nodust_Ha, ratio, Av_data, dered_Ha_hdul, dereddened_Ha)

plot_angles = [0, 2, 4]
for i in range(N_angles):
        sigma = 1.41
        sigma = None
        for d in ["dust"]: #, "nodust"]:
                Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                Hb = astropy.io.fits.open("results/final/em_Hb_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                nodust_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("nodust", i+1))

                if sigma != None:
                        Ha[0].data[0] = scipy.ndimage.gaussian_filter(Ha[0].data[0], sigma=sigma)
                        Hb[0].data[0] = scipy.ndimage.gaussian_filter(Hb[0].data[0], sigma=sigma)
                        nodust_Ha[0].data[0] = scipy.ndimage.gaussian_filter(nodust_Ha[0].data[0], sigma=sigma)

                Av_data = dereddenLib.A(dereddenLib.c_ext(Ha[0].data[0], Hb[0].data[0], intrinsic_ratio=3.0))
                Av_data[numpy.logical_not(mask[i])] = 0.0
                Av_maps["Ha_Hb", i] = Av_data
                dereddened_Ha = dereddenLib.deredden(Ha[0].data[0], lam=6563., Av=Av_data)

                Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
                Av_x_Ha_hdul[0].data[0] = Av_data * nodust_Ha[0].data[0]
                Av_x_Ha_hdul.writeto("results/final/AvHaHb_x_Ha0_%i.fits"%(i+1), overwrite=True)

                Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
                Av_x_Ha_hdul[0].data[0] = Av_data * dereddened_Ha
                Av_x_Ha_hdul.writeto("results/final/AvHaHb_x_deredHa_%i.fits"%(i+1), overwrite=True)


                Av_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                Av_hdul[0].data[0] = Av_data
                Av_hdul.writeto("results/final/Av_Ha_Hb_ratio_%i.fits"%(i+1), overwrite=True)

                dAv_hdul =  astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                dAv_hdul[0].data[0] = Av_maps["Ha_Ha0", i] - Av_maps["Ha_Hb", i]
                dAv_hdul.writeto("results/final/dAv_HaHa0_HaHb_ratio_%i.fits"%(i+1), overwrite=True)

                dered_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                dered_Ha_hdul[0].data[0] = dereddened_Ha
                dered_Ha_hdul.writeto("results/final/dered_Ha_HaHb_%i.fits"%(i+1), overwrite=True)

                Av_x_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_bare_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
                Av_x_Ha_hdul[0].data[0] = Av_data * Av_x_Ha_hdul[0].data[0]
                Av_x_Ha_hdul.writeto("results/final/AvHaHb_x_Ha_bare_dust_%i.fits"%(i+1), overwrite=True)


                ratio = Ha[0].data[0]/Hb[0].data[0]
                ratio[numpy.logical_not(mask[i])] = float(numpy.nan)

                ratio_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))

                ratio_hdul[0].data[0]= ratio
                ratio_hdul.writeto("results/final/Ha_Hb_ratio_%s_%i.fits"%(d,i+1), overwrite=True)

                ratio_dered_true = dereddened_Ha/nodust_Ha[0].data[0]
                ratio_dered_true_Ha_hdul = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
                ratio_dered_true_Ha_hdul[0].data[0] = ratio_dered_true
                ratio_dered_true_Ha_hdul.writeto("results/final/ratio_dered_true_Ha_%i.fits"%(i+1), overwrite=True)


                min_Ha = 1e6

                #ratio_hdul[0].data[0,:,:] = ratio
                #ratio_hdul.writeto("results/final/ratio.fits", overwrite=True)

                min_r = 2.8
                max_r = 14.8
                Nplus1 = int((max_r - min_r)*2)+1

                #plt.hist(numpy.log10(Ha[0].data[0,:,:][mask]), bins=100, range=[numpy.log10(min_Ha), numpy.max(numpy.log10(Ha[0].data[0,:,:]))])
                #plt.show()

                bins = numpy.linspace(min_r, max_r, Nplus1,endpoint=True)
                if i in plot_angles:
                        plot_index = plot_angles.index(i)
                        plt.hist(ratio[(ratio <= numpy.max(bins))*(ratio >= numpy.min(bins))], bins=bins, density=True, log=True, fc=color[plot_index], alpha=alpha[plot_index], ls=line_type[plot_index], lw=3, label=r"%i$^o$"%deg[i], zorder=zorder[plot_index])

#plt.title("%s ratio distribution"%d)
plt.xlabel(r"H$\alpha$/H$\beta$")
plt.ylabel("PDF")
plt.legend()
plt.savefig("results/final/plots/%s_ratio_distribution_%i.pdf"%(d, i+1))
plt.show()

import ds9_animate_lib
input("\nPlease start or adjust ds9. Press Enter to continue...")
#ds9_animate_lib.multi_panel_animation(["Ha_obs.lst", "Ha_nodust.lst", "Av_Ha_Hb_files.lst", "dAv.lst"], pause_to_adjust=True, savefile="multiplot_4panel", reverse_on_last=True)
ds9_animate_lib.multi_panel_animation(["Ha_obs.lst", "dered_Ha_HaHb.lst", "Ha_nodust.lst", "Av_Ha_Hb_files.lst", "dAv.lst", "ratio_dered_true.lst"], savefile="multiplot_6panel", pause_to_adjust=True, reverse_on_last=True)

        # plt.scatter(numpy.log10(Ha[0].data[0][:,:][mask]), numpy.log10(ratio[mask]))
        # plt.title("%s ratio vs ha %i.png"%(d, i))
        # plt.savefig("%s_ratio_vs_ha_%i.png"%(d, i))
        # plt.show()

