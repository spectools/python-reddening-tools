#!/usr/bin/python
from __future__ import print_function
import os
import sys
import numpy as np
sys.path.append(os.getcwd())
import dereddenLib as deredden
import deredden_noisy_fluxes

F1_intrinsic = 1.0
lam1 = 6563.0

F2_intrinsic = F1_intrinsic/2.86
lam2 = 4861.0

print("SN\tAv_act\tAv_rec\tF(Ha)_act\tF(Ha)_rec\tR(int,rec)\tsig(rec)\toffset")

for Av_actual in np.linspace(0.5, 5, num=10, endpoint=True):
    for F1StoN in np.linspace(5, 50, num=10, endpoint=True):

        F1_obs = deredden.redden(Flux=F1_intrinsic, lam=lam1, Av=Av_actual)
        F2_obs = deredden.redden(Flux=F2_intrinsic, lam=lam2, Av=Av_actual)
        sigF1 = F1_obs/F1StoN
        sigF2 = sigF1

        Av_recovered, sigAv, F1_recovered, sigF1_recovered = deredden_noisy_fluxes.montecarlo(F1_obs, sigF1, F2_obs, sigF2, N=int(1e4), doPlots=True)

        #print("Recovered Av = %0.2f +/- %0.2f (residual of %0.2f mag)"%(Av_recovered, sigAv, Av_recovered - Av_actual))
        #print("Recovered F(Ha) = %0.2e +/- %0.2e (systematic percent error %0.2e)"%(F1_recovered, sigF1_recovered, 100*(F1_recovered - F1_intrinsic)/F1_intrinsic))

        RecoveredToIntrinsic = F1_recovered/F1_intrinsic
        sig_recovered_over_intrinsic = sigF1_recovered/F1_intrinsic
        systematic_residual_err = (F1_recovered - F1_intrinsic)/F1_intrinsic

        print("%0.0f\t%0.3f\t%0.3f\t%0.3e\t%0.3e\t%0.3e\t%0.3e\t%0.3e\t"%(F1StoN, Av_actual, Av_recovered, F1_intrinsic, F1_recovered, RecoveredToIntrinsic, sig_recovered_over_intrinsic, systematic_residual_err))
    print("\n")

