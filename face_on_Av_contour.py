""" Calculate the Ha to Hb ratio of two images """
import astropy.io.fits
import numpy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import copy
import os
import sys
sys.path.append(os.environ["ASTRO_TOOLS"]+"/spectools/reddening")
import dereddenLib
import numpy

plt.rcParams["patch.force_edgecolor"] = True
font = {'family' : 'normal',
  'size' : 18}
matplotlib.rc('font', **font)

N_angles = 6
deg = numpy.linspace(0,15*(N_angles-1),N_angles)

#for i in range(3, 0, -1):
#range(1,4):

nodust_Ha_dig = 10**9
nodust_Ha_HII = 5*10**11

dusty_Ha_dig = 10**9
dusty_Ha_HII = 10**11

masks = {}
Av_maps = {}

ngridx = 200
ngridy = 100

Av_max_x = 20
Av_max_y = 5

for i in range(N_angles):
    dusty_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i))
    nodust_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("nodust", i))

    ratio = dusty_Ha[0].data[0]/nodust_Ha[0].data[0]

    Av_data = dereddenLib.A(dereddenLib.c_ext(ratio, 1.0, flamB=0, intrinsic_ratio=1.0))

    masks["dusty dim", i] = (dusty_Ha[0].data[0] > dusty_Ha_dig)
    masks["dusty dig", i] = (dusty_Ha[0].data[0] > dusty_Ha_dig)*(dusty_Ha[0].data[0] < dusty_Ha_HII)
    masks["dusty HII", i] = (dusty_Ha[0].data[0] > dusty_Ha_HII)

    masks["no dust dim", i] = (nodust_Ha[0].data[0] > nodust_Ha_dig)
    masks["no dust HII", i] = (nodust_Ha[0].data[0] > nodust_Ha_dig)*(nodust_Ha[0].data[0] < nodust_Ha_HII)
    masks["no dust HII", i] = (nodust_Ha[0].data[0] > nodust_Ha_HII)

    Av_data[Av_data < 0.0] = numpy.nan
    Av_maps["Ha_Ha0", i] = Av_data*1.0

    del(dusty_Ha, nodust_Ha, ratio, Av_data)

fig, ax = plt.subplots(nrows=3)

for i in [1,2,3]:
    for d in ["dust"]: #, "nodust"]:
        Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i))
        Hb = astropy.io.fits.open("results/final/em_Hb_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i))

        Av_data = dereddenLib.A(dereddenLib.c_ext(Ha[0].data[0], Hb[0].data[0]))
        Av_data[numpy.logical_not(masks["dusty dim", i])] = numpy.nan

        Av_data[Av_data < 0.0] = numpy.nan
        Av_maps["Ha_Hb", i] = Av_data*1.0

        dAv_data = Av_maps["Ha_Ha0", i] - Av_maps["Ha_Hb", i]

        mask = masks["no dust HII", i]
        im = ax[i-1].scatter(Av_maps["Ha_Ha0", i][mask], Av_maps["Ha_Hb", i][mask], label=r"HII %i$\degree$"%deg[i-1], c="red", s=1.0, alpha=0.2, zorder=10)
        im = ax[i-1].scatter(Av_maps["Ha_Ha0", i][numpy.logical_not(mask)], Av_maps["Ha_Hb", i][numpy.logical_not(mask)], c="blue", label=r"DIG %i$\degree$"%deg[i-1], s=1.0, alpha=0.2, zorder=5)

        ax[i-1].set_xlim((0,Av_max_x))
        ax[i-1].set_ylim((0,Av_max_y))
        leg = ax[i-1].legend()
        for text_i, text in enumerate(leg.get_texts()):
          plt.setp(text, color = ["red","blue"][text_i])
        #fig.colorbar(im, ax=ax[i-1])

        if i == 3:
            ax[i-1].set_xlabel(r"A$_V(true)$")
        if i == 2:
            ax[i-1].set_ylabel(r"$A_V(H\alpha, H\beta)$")


#plt.title("%s ratio distribution"%d)
#plt.savefig("Av_scatter.pdf")
plt.subplots_adjust(hspace=0.2)
plt.show()


