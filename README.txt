READ ME

requierments: scipy, numpy, matplotlib

Description of routines:

deredden_noisy_fluxes:
montecarlo(FHa, sigHa, FHb, sigHb, N=int(1e4), doPlots=False, Verbose=False):
    """ Montecarlo deredden noisy Ha and Hb fluxes """

All routines take either a float or an array of floats. 

It is not necessary that all inputs be either arrays or either floats, but all input arrays must be of the same length.

For example, you can (de)redden 
-- a list of fluxes with a single Av
--- or provide arrays of paired Av and Fluxes.
-- a list of wavelength and a single Av to get the differential extinction.

dereddenLib routines:
f(lam):
    """ Return the extiction curve at wavelength lam, normalized at V-band """
    flam: wavelength in angstroms

c_ext(fluxA, fluxB, lamA=float(6563), lamB=float(4861), intrinsic_ratio=float(2.86)):
    """ Return C, the extiction decrement. Default assumes Ha and Hb fluxes, but accepts any two line ratios with known intrinsic ratio"""
    C is returned after supplying fluxes and wavelengths for A and B, as well as the intrinsic ratio of fluxA/fluxB. Order matters.
    fluxA, fluxB: Emission line fluxes
    lamA, lamB: wavelengths in angstroms of line fluxes.
    intrinsic_ratio: unreddended intrinsic ratio of fluxA/fluxB

A(c_ext, lam=5555.0):
    """Extinction in magnitues from c. Wavelenght default is 5555Ang, or V-band"""
    This routine retuns the extinction of C in magnitudes at a given wavelenght. This defaults to V band, returning Av

deredden(Flux, lam, c_ext=1.0, Av=-1.0):
    """Deredden a line flux "Flux" at wavelength "lam" using either c or Av, but not both."""
    Provided a Flux wavelength and amount of extinction via c_ext or Av, return a dereddened line flux

redden(Flux, lam, c_ext=1.0, Av=-1.0):
    """Deredden a line flux "Flux" at wavelength "lam" using either c or Av, but not both."""
    The inverse of dereddenLib.deredden, redden a line flux.

