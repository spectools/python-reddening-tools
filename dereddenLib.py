"""Functions that redden and deredden data using an R=E(B-V)/Av=3.1"""
from scipy.interpolate import UnivariateSpline
import numpy
import sys
# pylint: disable=invalid-name

"""This generates the f_lamda curves of relative extinction, normalized at V band"""
# Wavelengths reported in AGN3 - Table 7.1
lam_array = numpy.array([3.3333e4, 2e4, 1.8761e4, 1.25e4, 1e4, 8333, 7143, 6563, 6250,\
 5555, 5e3, 4861, 4545, 4340, 4167, 3846, 3571, 3333, 2941])
R3_array = numpy.array([0.058, 0.132, 0.147, 0.282, 0.404, 0.552, 0.728, 0.818, 0.866,\
 0.987, 1.122, 1.164, 1.271, 1.346, 1.409, 1.509, 1.575, 1.643, 1.851])
inv_lam_array = 1 / lam_array

# Perform a split fit to the R=3.1 curve in inverse wavelength space
# EWP 10/6/2017 - Changed default degree of smoothing. 3 is just a bit rough
spline = UnivariateSpline(inv_lam_array, R3_array, k=4)

def f(lam):
    """ Return the extiction curve at wavelength lam, normalized at V-band """
    if isinstance(lam, float) or isinstance(lam, str):
        lam = numpy.array([float(lam)])

    elif isinstance(lam, list):
        lam = numpy.array(lam)

    result = spline(1.0/lam).base
    
    if len(result) == 1:
        return result[0]
    else:
        return result

        

def c_ext(fluxA, fluxB, lamA=float(6563), flamA=None, lamB=float(4861), flamB=None, intrinsic_ratio=float(2.86)):
    """ Return C, the extiction decrement. Default assumes Ha and Hb fluxes, but accepts any two line ratios with known intrinsic ratio"""
    if flamA == None:
        flamA = f(lamA)
    if flamB == None:
        flamB = f(lamB)

    if isinstance(fluxA, float) and isinstance(fluxB, float):
        c_ext = numpy.log10((fluxA/fluxB)/intrinsic_ratio) / (flamA - flamB)

    else:
        if isinstance(fluxA, list):
            fluxA = numpy.asarray(fluxA)
        
        elif isinstance(fluxA, float):
            fluxA = numpy.array([fluxA])

        if isinstance(fluxB, list):
            fluxB = numpy.asarray(fluxB)
        elif isinstance(fluxB, float):
            fluxB = numpy.array([fluxB])

        if len(fluxA) != len(fluxB) and ( (len(fluxA) != 1) and (len(fluxB) !=1 ) ):
            sys.exit("dereddenlib:c: Lengths of A and B not equal]\n")

        c_ext = numpy.log10((fluxA/fluxB)/intrinsic_ratio) / (flamA - flamB)

    return c_ext



def A(c_ext, lam=5555.0):
    """Extinction in magnitues from c. Wavelenght default is 5555Ang, or V-band"""
    # c_ext = numpy.array(c_ext)
    # lam = numpy.array(lam)
    # max_length = max(len(c_ext), len(lam))

    # dict ={"lam":lam,"c_ext":c_ext}
    # things = list(dict.keys())
    # for i in range(len(things)):
    #     ref = dict[things[i]]
    #     if len(ref) not in set(1,max_length):
    #         sys.exit("Arrays miss matched in length")
    #     elif len(ref) == 1:
    #         ref = numpy.full(max_length, ref[0])
    return -2.5 * c_ext * f(lam)

def deredden(Flux, lam, c_ext=None, Av=None):
    """Deredden a line flux "Flux" at wavelength "lam" using either c or Av, but not both."""
    if (c_ext != None) and (Av != None):
        sys.exit("You can't specify both c_ext and Av")

    length_elements = numpy.array([])

    input_dict = {"Flux":Flux, "lam":lam, "c_ext":c_ext, "Av":Av}

    # Check if each input is length 1/float or, if an array, that it has the same length as all the other array
    for k in input_dict.keys():
        if input_dict[k] is not None:
            if ( isinstance(input_dict[k], float) or isinstance(input_dict[k], int) ):
                input_dict[k] = numpy.array([input_dict[k]])
            length_elements = numpy.append(length_elements, len(input_dict[k]))

    # if isinstance(Flux, float):
    #     Flux = numpy.array([Flux])
    # length_elements = numpy.append(length_elements, len(Flux))
        
    # if isinstance(Av, float):
    #     Av = numpy.array([Av])
    # length_elements = numpy.append(length_elements, len(Av))

    # if isinstance(c_ext, float):
    #     c_ext = numpy.array([c_ext])
    # length_elements = numpy.append(length_elements, len(c_ext))

    # if isinstance(lam, float):
    #     lam = numpy.array([lam])
    # length_elements = numpy.append(length_elements, len(lam))

    if( len(length_elements) != len(length_elements[(length_elements== 1) + (length_elements == numpy.max(length_elements))])):
        sys.exit("Error: Flux, Av/Cext and lam must be combinations of floats (or array len=1) and arrays of the same length.")

    if Av is not None:
        """Then correct flux using Av"""
        return (input_dict["Flux"] * 10**(input_dict["Av"]/2.5*f(input_dict["lam"]))) 
    elif c_ext is not None:
        """Correct flux using c_ext"""
        return (input_dict["Flux"] * 10**(-1.0*input_dict["c_ext"]*f(input_dict["lam"])))

def redden(Flux, lam, c_ext=1.0, Av=-1.0):
    """Redden a line flux "Flux" at wavelength "lam" using either c or Av, but not both."""
    # if isinstance(Flux, float):
    #     Flux = numpy.array([Flux])
    # if isinstance(Av, float):
    #     Av = numpy.array([Av])
    # if isinstance(c_ext, float):
    #     c_ext = numpy.array([c_ext])
    # if isinstance(lam, float):
    #     lam = numpy.array(lam)

    # if (Av[0] != -1.0) and (c_ext[0] != 1.0):
    #     sys.exit("dereddenLib:deredden: You can not set Av and c_ext at the same time")

    # max_length = max(len(Flux), len(Av), len(c_ext), len(lam))

    # dict ={"Flux":Flux,"lam":lam,"Av":Av, 'c_ext':c_ext}
    # things = list(dict.keys())
    # for i in range(4):
    #     ref = dict[things[i]]
    #     if len(ref) not in set(1,max_length):
    #         sys.exit("Arrays miss matched in length")
    #     elif len(ref) == 1:
    #         ref = numpy.full(max_length, ref[0])

    """ Return reddened flux F1, of a line at wavelength lam, with decrement c or extinction A"""
    flam = f(lam)
    # First if: Is Av physical and c_ext== default nonphysical value
    if (Av >= 0) and (c_ext == 1.0):
        return Flux * 10**(-1.*Av*flam/2.5)
    # Second if: Is c_ext physical and Av == default nonphysical value
    elif (c_ext <= 0) and (Av == -1.0):
        return float(Flux * 10**(c_ext*flam))
    else:
        return numpy.nan
