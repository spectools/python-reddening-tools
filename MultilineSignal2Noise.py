#!/usr/bin/python
from __future__ import print_function
import os
import sys
import numpy as np
sys.path.append(os.getcwd())
import dereddenLib as deredden
import deredden_noisy_fluxes
#import model
# import etc

F1_intrinsic = float(1.0)
lam1 = float(6563.0)
label1 = "Ha"

F2_intrinsic = F1_intrinsic/2.86
lam2 = float(4861.0)
label2 = "Hb"

F2_intrinsic = F1_intrinsic/2.86/5
lam2 = float(10000.0)
label2 = "PaDelta"

["Ha"]
["6563"]

OUT  = open("%s_%s_noisy_dered.dat"%(label1, label2), "w")

OUT.writelines("#SN\tAv_act\tAv_rec\tF("+label1+")_obs\tF("+label1+")_act\tF("+label1+")_rec\tR(int,rec)\tsig(rec)\toffset\n")
    
#for Av_actual in np.linspace(0.5, 5, num=10, endpoint=True):
for Av_actual in [2, 2.5, 3, 3.5, 4, 6, 8]:
    for F1StoN in np.linspace(5, 50, num=10, endpoint=True):

        F1_obs = deredden.redden(Flux=F1_intrinsic, lam=lam1, Av=Av_actual)
        F2_obs = deredden.redden(Flux=F2_intrinsic, lam=lam2, Av=Av_actual)
        sigF1 = F1_obs/F1StoN
        sigF2 = sigF1*4
        
        Av_recovered, sigAv, F1_recovered, sigF1_recovered = deredden_noisy_fluxes.montecarlo(F1_obs, sigF1, F2_obs, sigF2, lam1=lam1, label1=label1, label2=label2, lam2=lam2, intrinsic_ratio=F1_intrinsic/F2_intrinsic, N=int(1e4), doPlots=True)

        #print("Recovered Av = %0.2f +/- %0.2f (residual of %0.2f mag)"%(Av_recovered, sigAv, Av_recovered - Av_actual))
        #print("Recovered F(Ha) = %0.2e +/- %0.2e (systematic percent error %0.2e)"%(F1_recovered, sigF1_recovered, 100*(F1_recovered - F1_intrinsic)/F1_intrinsic))

        RecoveredToIntrinsic = F1_recovered/F1_intrinsic
        sig_recovered_over_intrinsic = sigF1_recovered/F1_intrinsic
        systematic_residual_err = (F1_recovered - F1_intrinsic)/F1_intrinsic

        OUT.writelines("%0.0f\t%0.3f\t%0.3f\t%0.3f\t%0.3e\t%0.3e\t%0.3e\t%0.3e\t%0.3e\n"%(F1StoN, Av_actual, Av_recovered, F1_obs, F1_intrinsic, F1_recovered, RecoveredToIntrinsic, sig_recovered_over_intrinsic, systematic_residual_err))
    
    OUT.writelines("\n")
OUT.close()
