import numpy as np
import matplotlib.pyplot as plt
import astropy.io.ascii as ascii

t = ascii.read("Ha_PaDelta_noisy_dered.dat")

for i, Av in enumerate(np.unique(t['Av_act'])):
    mask = t['Av_act'] == Av
    plt.plot(t['SN'][mask], 1/t["R(int,rec)"][mask], label="%0.1f"%Av, linewidth=float(i+1), zorder=-i)

plt.ylabel("Recovered S/N in H$\\alpha$")
plt.xlabel("Observed S/N in H$\\alpha$")
plt.title("H$\\alpha$ recovered using Pa-$\\delta$")
plt.legend(title="Av")
plt.savefig("Ha_PaDelta_noisy_dered.pdf")
plt.close()

t = ascii.read("Ha_Hb_noisy_dered.dat")

for i, Av in enumerate(np.unique(t['Av_act'])):
    mask = t['Av_act'] == Av
    plt.plot(t['SN'][mask], 1/t["R(int,rec)"][mask], label="%0.1f"%Av, linewidth=float(i+1), zorder=-i)

plt.ylabel("Recovered H$\\alpha$/True")
plt.xlabel("Observed S/N in H$\\alpha$")
plt.title("H$\\alpha$ recovered using H$\\beta$")
plt.ylim([1,5])
plt.legend(title="Av")
plt.savefig("Ha_Hb_noisy_dered.pdf")
plt.close()