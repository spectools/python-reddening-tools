#!/usr/bin/python3
"""This module calculates the total uncertainty on dereddened Ha fluxes provided a Ha and Hb
   Relative uncertainties and an Av are required in myconfig """
# pylint: disable=invalid-name
import numpy as np
import dereddenLib as deredden
import matplotlib.pyplot as plt

def montecarlo(F1, sig1, F2, sig2, label1="Ha", label2="Hb", lam1=6563, lam2=4861, intrinsic_ratio=2.89, N=int(1e4), doPlots=False, Verbose=False):
    """ Montecarlo deredden noisy Ha and Hb fluxes """
    # Create array containing random F1 and F2 values

    F1_pop = F1 + sig1 * np.random.randn(N)
    F2_pop = F2 + sig2 * np.random.randn(N)
    c_true = deredden.c_ext(F1, F2, lamA=lam1, lamB=lam2, intrinsic_ratio=intrinsic_ratio)
    Av_true = deredden.A(c_true)
    Hascale = deredden.deredden(1.0, lam1, c_ext=c_true)


    Av_pop = np.zeros(N) - 1
    c_ext_pop = np.zeros(N) - 1
    F1_recovered_pop = np.zeros(N) - 1
    F2_recovered_pop = np.zeros(N) - 1

    for i in range(N):
        c_ext_pop[i] = deredden.c_ext(F1_pop[i], F2_pop[i], lamA=lam1, lamB=lam2, intrinsic_ratio=intrinsic_ratio)
        Av_pop[i] = deredden.A(c_ext_pop[i],lam=5555.0)
        if np.isfinite(c_ext_pop[i]):
            F1_recovered_pop[i] = deredden.deredden(F1_pop[i], 6563, c_ext=c_ext_pop[i])
            F2_recovered_pop[i] = deredden.deredden(F2_pop[i], 4861, c_ext=c_ext_pop[i])
        else:
            F1_recovered_pop[i] = np.nan
            F2_recovered_pop[i] = np.nan

    # NEW!!!
    #"""Reddening routines work on arrays of data. No need to loop"""
    #c_ext_pop = deredden.c_ext(F1_pop, F2_pop)
    #Av_pop    = deredden.A(c_ext_pop,lam=5555.0)
    #F1_recovered_pop = deredden.deredden(F1_pop, 6563.0, c_ext=c_ext_pop)
    #F2_recovered_pop = deredden.deredden(F2_pop, 4861.0, c_ext=c_ext_pop)


    Av_recovered = np.nanmedian(Av_pop)
    sigAv_recovered = np.nanstd(Av_pop)
    F1_recovered = np.nanmedian(F1_recovered_pop)
    sig1_recovered = np.nanstd(F1_recovered_pop)
    if doPlots is True:
        plt.hist(np.log10(F1_recovered_pop[np.isfinite(F1_recovered_pop)]), bins='auto', label="Recovered")
        plt.hist(np.log10(Hascale * F1_pop[np.isfinite(F1_pop)]), bins='auto', color="black", label="$\sigma_{observed}$", fill=True)
        plt.title("%s Recovered Dereddened Flux\n Av = %0.1f; S/N(%s) = %0.0f; S/N(%s) = %0.0f"%(label1, Av_true, label1, F1/sig1, label2, F2/sig2))
        plt.xlabel('$Log_{10}\ Dereddened\ F(%s)_{recovered}/F(%s)_{true}$'%(label1, label1))
        plt.ylabel('PDF')
        plt.legend()
        #plt.show()
        
        plt.savefig("plots/%s_%s_Av_%2.1f_%sSN_%1.0f.png"%(label1, label2, Av_true, label1, F1/sig1))
        plt.close()

    return(Av_recovered, sigAv_recovered, F1_recovered, sig1_recovered)

def multiLine_montecarlo(FHa, sigHa, FHb, sigHb, Lines, intratio=2.9, N=int(1e4), doPlots=False, Verbose=False):
    """Given a true observed F(Ha) and F(Hb) what are the recovered deredden fluxes. Usage multiLine_montecarlo(FHa, sigHa, FHb, sigHb, Lines, *doPlots=False, *Verbose=False)"""
    """Lines[0] = lambda of lines"""
    """Lines[1] = fluxes of lines"""
    """Lines[2] = noise of lines"""

    """ Montecarlo deredden noisy Ha and Hb fluxes """
    # Create array containing random FHa and FHb values
    FHa_pop = FHa + sigHa * np.random.randn(N)
    FHb_pop = FHb + sigHb * np.random.randn(N)
    Lines_pop = np.ndarray((len(Lines[1]), N))
    for line_n in range(len(Lines[0])):
        # I do this in a loop because I am a bad programer, but I don't want to use the same random number to scatter all lines in the same montecarlo run
        # realization of Line flux = Line_flux + uncertainty*np.random
        Lines_pop[line_n] = Lines[1][line_n] + Lines[2][line_n] * np.random.randn(N)

    """Calculate the true extinction"""
    c_true = deredden.c_ext(FHa, FHb, intrinsic_ratio=intratio)
    Av_true = deredden.A(c_true)

    """Calculate the relative extinction of H-alpha"""
    Hascale = deredden.deredden(1.0, 6563, c_ext=c_true)

    """ Initialize arrays to store our recovered values"""
    Av_pop = np.full(N, -1.0)
    c_ext_pop = np.full(N, -1.0)
    FHa_recovered_pop = np.full(N, -1.0)
    FHb_recovered_pop = np.full(N, -1.0)

    """Create an array to store recovered line fluxes. This can be used to calculate stddev's or ratios, and then stdev"""
    Lines_recovered_pop = np.full([len(Lines[0]), N], -1.0)

    for i in range(N):
        c_ext_pop[i] = deredden.c_ext(FHa_pop[i], FHb_pop[i], intrinsic_ratio=intratio)
        Av_pop[i] = deredden.A(c_ext_pop[i],lam=5555.0)
        if np.isfinite(c_ext_pop[i]):
            FHa_recovered_pop[i] = deredden.deredden(FHa_pop[i], 6563, c_ext=c_ext_pop[i])
            for line_n in range(len(Lines[0])):
                Lines_recovered_pop[line_n][i] = deredden.deredden(Lines_pop[line_n][i], Lines[0][line_n], c_ext=c_ext_pop[i])
        else:
            FHa_recovered_pop[i] = np.nan
            for line_n in range(len(Lines[0])):
                Lines_recovered_pop[line_n][i] = np.nan

    """Calculate the population of recovered line ratios"""
    recovered_Ratio_pop = np.ndarray((len(Lines[0]), len(Lines[0]), N))
    for j in range(len(Lines[0])):
        for k in range(len(Lines[0])):
                #print(np.nanmedian(Lines_recovered_pop[j]/Lines_recovered_pop[k]))
                #print(np.nanstd(Lines_recovered_pop[j]/Lines_recovered_pop[k]))
                recovered_Ratio_pop[j][k] = np.array(Lines_recovered_pop[j])/np.array(Lines_recovered_pop[k])
                
    Av_recovered = np.nanmedian(Av_pop)
    sigAv_recovered = np.nanstd(Av_pop)
    FHa_recovered = np.nanmedian(FHa_recovered_pop)
    sigHa_recovered = np.nanstd(FHa_recovered_pop)

    FLines_recovered = np.ndarray(len(Lines[0]))
    sigFLines_recovered = np.ndarray(len(Lines[0]))
    for line_n in range(len(Lines[0])):
        FLines_recovered[line_n] = np.nanmedian(Lines_recovered_pop[line_n])
    for line_n in range(len(Lines[0])):
        sigFLines_recovered[line_n] = np.nanstd(Lines_recovered_pop[line_n])

    Line_ratios_recovered =  np.ndarray([len(Lines[0]),len(Lines[0])])
    sigLine_ratios_recovered =  np.ndarray([len(Lines[0]),len(Lines[0])])
    for j in range(len(Lines[0])):
        for k in range(len(Lines[0])):
                Line_ratios_recovered[j][k] = np.nanmedian(recovered_Ratio_pop[j][k])
                sigLine_ratios_recovered[j][k] = np.nanstd(recovered_Ratio_pop[j][k])


    if doPlots is True:
        plt.hist(np.log10(FHa_recovered_pop[np.isfinite(FHa_recovered_pop)]), bins='auto', label="Recovered")
        plt.hist(np.log10(Hascale * FHa_pop[np.isfinite(FHa_pop)]), bins='auto', color="black", label="$\sigma_{observed}$", fill=True)
        plt.title("Halpha Recovered Dereddened Flux\n Av = %0.1f; S/N(Ha) = %0.0f; S/N(Hb) = %0.0f"%(Av_true, FHa/sigHa, FHb/sigHb))
        plt.xlabel('$Log_{10}\ Dereddened\ F(Ha)_{recovered}/F(Ha)_{true}$')
        plt.ylabel('PDF')
        plt.legend()
        #plt.show()
        
        plt.savefig("plots/Av_%2.1f_SN_%1.0f.png"%(Av_true, FHa/sigHa))
        plt.close()

    return(Av_true, Av_recovered, sigAv_recovered, FHa_recovered, sigHa_recovered, FLines_recovered, sigFLines_recovered, Line_ratios_recovered, sigLine_ratios_recovered, recovered_Ratio_pop)
