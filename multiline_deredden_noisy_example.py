#!/usr/bin/python3
"""Test multiline deredden"""
import deredden_noisy_fluxes as dered
import dereddenLib

FHa = 400.0; sigHa = 2.0
FHb = 100.0; sigHb = 2.0
Lines=[[3727., 5007., 6563., 6720.], [10.0, 200.0, 3000.0, 300.0], [0.1, 0.1, 0.1, 0.1]]

# ## The above are assumed to be reddened fluxes. If they are not reddened you can redden them
# ## by uncommenting the lower lines. You can compare recovered dereddened fluxes to unreddened_true_Lines when you are done 
# unreddened_true_Lines = Lines
# FHa = 286; sigHa = 0.1
# FHb = 100; sigHa = 0.1

# Av = 0.5

# for line_n in range(len(Lines[0])):
#     #redden(Flux, lam, Av=Av)
#     lam  = Lines[0][line_n]5
#     flux = Lines[1][line_n]    
#     unc  = Lines[2][line_n]
#     Lines[1][line_n] = dereddenLib.redden(flux, lam, Av=Av)

# FHa = dereddenLib.redden(FHa, 6563., Av=Av)
# FHb = dereddenLib.redden(FHb, 4861., Av=Av)
# sigHa = dereddenLib.redden(sigHa, 6563., Av=Av)
# sigHa = dereddenLib.redden(sigHb, 4861., Av=Av)

Av_true, Av_recovered, sigAv_recovered, FHa_recovered, sigHa_recovered, FLines_recovered, sigFLines_recovered, Line_ratios_recovered, sigLine_ratios_recovered, recovered_Ratio_pop = dered.multiLine_montecarlo(FHa, sigHa, FHb, sigHb, Lines=Lines)

print("Av = %0.2f"%(Av_true))
print("Av recovered = %0.2f +/- %0.3f"%(Av_recovered, sigAv_recovered))

print("reddened Ha observed = %0.2f"%FHa)
print("dereddened Ha recovered = %0.2f +/- %0.3f"%(FHa_recovered, sigHa_recovered))

print("Lines = [Wave] [Flux] [Noise]")
print(Lines)

print("Recovered Line Ratios Matrix")
print(Line_ratios_recovered)

print("Recovered Line Ratio  uncertainties")
print(sigLine_ratios_recovered)
