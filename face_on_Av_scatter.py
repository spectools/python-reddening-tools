""" Calculate the Ha to Hb ratio of two images """
import astropy.io.fits
import numpy
import matplotlib
import matplotlib.pyplot as plt
import copy
import os
import sys
sys.path.append(os.environ["ASTRO_TOOLS"]+"/spectools/reddening")
import dereddenLib
import numpy

plt.rcParams["patch.force_edgecolor"] = True
font = {'family' : 'normal',
  'size' : 18}
matplotlib.rc('font', **font)

N_angles = 6

deg = numpy.linspace(0,15*(N_angles-1),N_angles)

alpha = [.8, .8, 0.5]
zorder = [0, 5, 10]
line_type = ['-', '-.', 'dotted' ]
color = [(0,0,1,0.5), (1,0,0,0.5), (1.0,1.0,1.0, 0.5)]

mask = {}
Av_maps = {}

for i in range(N_angles): 
    dusty_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("dust", i+1))
    nodust_Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%("nodust", i+1))

    ratio = dusty_Ha[0].data[0]/nodust_Ha[0].data[0]

    Av_data = dereddenLib.A(dereddenLib.c_ext(ratio, 1.0, flamB=0, intrinsic_ratio=1.0))

    mask[i] = (dusty_Ha[0].data[0] > 1e7)*(Av_data >= 0.0)

    Av_data[Av_data < 0.0] = numpy.nan
    Av_maps["Ha_Ha0", i] = Av_data*1.0

    del(dusty_Ha, nodust_Ha, ratio, Av_data)

plot_angles = [0, 2, 4]
for i in plot_angles:
    for d in ["dust"]: #, "nodust"]:
        Ha = astropy.io.fits.open("results/final/em_Ha_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))
        Hb = astropy.io.fits.open("results/final/em_Hb_%s/int_channel_map_species_0001_line_000%i.fits"%(d, i+1))

        Av_data = dereddenLib.A(dereddenLib.c_ext(Ha[0].data[0], Hb[0].data[0]))
        Av_data[numpy.logical_not(mask[i])] = numpy.nan
        Av_maps["Ha_Hb", i] = Av_data*1.0

        dAv_data = Av_maps["Ha_Ha0", i] - Av_maps["Ha_Hb", i]

        plt.scatter(Av_maps["Ha_Ha0", i], Av_maps["Ha_Hb", i], label=r"%i$\degree$"%deg[i], s=0.3, alpha=0.2)

        min_r = 2.8
        max_r = 14.8
        Nplus1 = int((max_r - min_r)*2)+1

#plt.title("%s ratio distribution"%d)
plt.xlim(left=0)
plt.ylim(bottom=0)
plt.xlabel(r"A$_V(true)$")
plt.ylabel(r"$A_V(H\alpha, H\beta)$")
plt.legend()
#plt.savefig("Av_scatter.pdf")
plt.show()


