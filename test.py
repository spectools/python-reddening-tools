#!/usr/bin/python
from __future__ import print_function
import os
import sys
import argparse
sys.path.append(os.getcwd())
import dereddenLib as deredden
import deredden_noisy_fluxes


print("Test: flam(Ha) = ", deredden.f(6563))

FHALPHA = [3.0, 4.0, 5.0]
FHBETA = [1.0, 1.0, 1.0]
CEXT = deredden.c_ext(FHALPHA, FHBETA)
A = deredden.A(c_ext=CEXT)
if isinstance(FHALPHA, float):
    print("Test: c(%0.2f, %0.2f) = %0.2f"%(FHALPHA, FHBETA, CEXT))
    print("Test: A(c=%0.2f) = %0.2f"%(CEXT, A))

    print("Monte Carlo a 5 sigma Ha")

Av_actual = 5.0
FHa_intrinsic = 1e-14
FHb_intrinsic = FHa_intrinsic/2.86
HaStoN= 50.0

FHa_obs = deredden.redden(Flux=FHa_intrinsic, lam=6563.0, Av=Av_actual)
sigHa = FHa_obs/HaStoN
sigHb = sigHa
FHb_obs = deredden.redden(Flux=FHa_intrinsic/2.86, lam=4861.0, Av=Av_actual)
print("Intrinsic Hb flux of %0.2e, reddened by Av = %0.2f is F(Hb,obs) = %0.2e"%(FHb_intrinsic, Av_actual, FHb_obs))


FHb_dered = deredden.deredden(FHb_obs,lam=4861.0,)
print("Dereddened Hbeta = %0.2e"%(FHb_intrinsic))

Av, sigAv = deredden_noisy_fluxes.montecarlo(FHa_obs, sigHa, FHb_obs, sigHb)

print("Recovered Av = %0.2f +/- %0.2f (residual of %0.2f"%(Av, sigAv, Av))
