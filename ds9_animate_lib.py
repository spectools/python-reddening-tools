import subprocess
import sys
import numpy

def xpaset(args):
    cmd = "%s"%(" ".join(["xpaset", "-p", "ds9"]+args))
    subprocess.run(["bash", "-c", cmd])

# def xpaset(args):
#     subprocess.run(["xpaset", "-p", "ds9"]+args)

def xpaget(args):
    cmd = "%s"%(" ".join(["bash", "-c" "xpaget", "ds9"]+args))
    output = subprocess.getoutput(cmd)
    return(output)

def lock(args):
    xpaset(["lock"]+args)

def read_filelist(filename):
    files = []
    labels = []

    f = open(filename, 'r')
    rows = f.read().split("\n")
    for row in rows:
        parts = row.split("\t")
        if len(parts[0]) > 0:
            files.append(parts[0])

        if len(parts) == 2:
            labels.append(parts[1])
        else:
            """This ensures len files and lables are the same"""
            labels.append("")

    f.close()
    return([files, labels])

def load_filelist(filenames, labels=None, deleteall=False):
    if deleteall == True:
        xpaset(["frame", "delete", "all"])

    frame_i = 0
    for filename in filenames:
        frame_i += 1
        xpaset(["frame", "frameno", "%i"%(frame_i)])
        xpaset(["fits", filename])

def save_frames(save_format="jpeg", savename="ds9", quality=100, pause_to_adjust=False, reverse_on_last=False):
    if pause_to_adjust == True:
        input("Please adjust frames. Press Enter to continue...")
    xpaset(["frame", "last"])
    last_frameno = xpaget(["frame"])
    image_j = 0
    for frame_i in range(1,int(last_frameno)+1):
        image_j += 1
        xpaset(["frame", "frameno", "%i"%(frame_i)])
        xpaset(["saveimage", save_format, "ds9-%04i.%s"%(image_j, save_format), "%i"%(quality)])
    if reverse_on_last:
        for frame_i in range(int(last_frameno), 1, -1):
            image_j += 1
            xpaset(["frame", "frameno", "%i"%(frame_i)])
            xpaset(["saveimage", save_format, "ds9-%04i.%s"%(image_j, save_format), "%i"%(quality)])

def multi_panel_load(file_list_dict, slice_i):
    """ Take a list of a list of files, and load the slice_i'th file into a list of open frames """
    for frame_j in file_list_dict.keys():
        xpaset(["frame", "frameno", "%i"%(frame_j)])
        xpaset(["fits", file_list_dict[frame_j][slice_i]])

def multi_panel_animation(file_lists, savefile=None, nx_ny=None, pause_to_adjust=False, save_format="jpeg", quality=100, reverse_on_last=False, zoom="fit", pause_between_frames=False):
    """Scale, color and contrast are preserved upon loading a new image. So 
    each frames will load the first image in the list, then the user adjusts them.
    The code will then proceed to save/load/save/load each file in each tile."""

    if savefile == None:
        savefile = "ds9-multipanel"

    if nx_ny != None:
        """ If nx_ny is specified used the specific grid layout, otherwise use default"""
        if len(file_lists) > nx_ny[0] * nx_ny[1]:
            sys.exit("The number of files does not match the number of panels: n(file lists)=%i, n(panels) = %i x %i = %i"%(len(file_lists), nx_ny[0], nx_ny[1], nx_ny[0]*nx_ny_[1]))
        xpaset(["tile", "grid", "layout", "%i"%nx_ny[0], "%i"%nx_ny[1]])
    else:
        xpaset(["tile", "grid"])
    xpaset(["tile"])

    lock(["frame", "wcs"])

    file_list_dictionary = {}

    for panel_i in range(1, len(file_lists)+1):
        #Read the file list. Remember index 0 is file names. Index 1 is labels
        file_list_dictionary[panel_i] = read_filelist(file_lists[panel_i-1])[0]

    multi_panel_load(file_list_dictionary, 1)
    if pause_to_adjust == True:
        input("Please adjust each tile contrast, brightness and color. Press Enter to continue...")

    save_j = 0
    for slice_i in range(len(file_list_dictionary[1])):
        multi_panel_load(file_list_dictionary, slice_i)
        if zoom == "fit":
            xpaset(["zoom", "to fit"])
        if pause_between_frames:
            input("Press Enter to continue...")
        xpaset(["saveimage", save_format, "%s-%04i.%s"%(savefile, save_j, save_format), "%i"%(quality)])
        save_j += 1

    if reverse_on_last:
        for slice_i in range(len(file_list_dictionary)):
            save_index = (len(file_list_dictionary) + slice_i)
            multi_panel_load(file_list_dictionary, len(file_list_dictionary)-1 - slice_i)
            if zoom == "fit":
                xpaset(["zoom", "to fit"])
            xpaset(["saveimage", save_format, "%s-%04i.%s"%(savefile, save_j, save_format), "%i"%(quality)])
            save_j += 1

# def ellipse(x, y, dx0, dy0, dx1, dy1, N_rings, phi=None, theta=0, frame="image"):
#     command = "'{ellipse %f,%f"
#     for n in range(N_rings):
#         x_n = float(dx0) + n * (float(dx1) - float(dx0))
#         y_n = float(dy0) + n * (float(dy1) - float(dy0))
#         if phi != None:
#             y_n *= numpy.cos(numpy.deg2rad(phi))
#         command += "%f,%f"%(x_n, y_n)
#     command += "%f}"%(theta)

def ellipse_phot(x, y, dx0, dy0, dx1, dy1, N_rings, phi=None, theta=0):
    command = "'{ellipse %f,%f"
    for n in range(N_rings):
        x_n = float(dx0) + n * (float(dx1) - float(dx0))
        y_n = float(dy0) + n * (float(dy1) - float(dy0))
        if phi != None:
            y_n *= numpy.cos(numpy.deg2rad(phi))
        command += "%f,%f"%(x_n, y_n)
    command += "%f}"%(theta)

if __name__ == "__main__":
    load_filelist(read_filelist("files.lst")[0], deleteall=True)
    lock(["frame", "wcs"])
    lock(["scalelimits"])
    lock(["colorbar"])
    xpaset(["scale", "limits", "5e8", "1e12"])
    save_frames(pause_to_adjust=True, reverse_on_last=True)
